package ru.shemplo.dm.difschemes

enum class SimulationProfiles constructor(private val NAME: String, private val DITRIBUTION: (Double) -> Double) {


    STEP("Ступенька", {
        if (it in 0.35..0.65) 1.0 else 0.0
    }),
    PEEK("Пик", {
        if (it in 0.475..0.5)
            (it - 0.475) / 0.025
        else if (it > 0.5 && it <= 0.525)
            (0.525 - it) / 0.025
        else
            0.0
    }),
    SINUSOID("Синусоида", {
        if (it in 0.25..0.75)
            Math.sin((2 * Math.PI) * ((it - 0.25) / 0.5))
        else
            0.0
    }),
    HEARTBIT("Единичное колебание", { p ->
        if (p in 0.45..0.55)
            if (p < 0.5)
                Math.abs(Math.abs((p - 0.475) / 0.025) - 1)
            else
                Math.abs((p - 0.525) / 0.025) - 1
        else
            0.0
    }),
    LINE("Полюсы", { it - 0.5 }),
    SPLASH("Единичный импульс", {
        if ((10 * it >= 3 * Math.PI / 2 && 10 * it <= 5 * Math.PI / 2))
            Math.cos(10 * it)
        else
            0.0
    })
    ;

    override fun toString(): String = NAME

    fun getProfile(points: Int): DoubleArray {
        var points = points
        points = Math.min(1000, points)

        val dist = DoubleArray(points)
        for (i in 0 until points) {
            dist[i] = DITRIBUTION(i.toDouble() / points)
        }

        return dist
    }
}