package ru.shemplo.dm.difschemes.logic

import ru.shemplo.dm.difschemes.logic.DifferenceScheme.Scheme

@Scheme(name = "Явная центральная")
class DifSchemeExplicitCentral(zeroLayer: DoubleArray, its: Int, u: Double, k: Double, dt: Double, dx: Double)
    : AbsDifferenceScheme(zeroLayer, its, u, k, dt, dx) {

    override fun doUnexistingStep(step: Int, profile: DoubleArray) {
        val previous = arrayOf(getTimeLayer(step - 1))
        for (i in 1 until profile.size - 1) {
            profile[i] = (previous[0][i] * (1 - 2 * R)
                    + previous[0][i - 1] * (R + S / 2)
                    + previous[0][i + 1] * (R - S / 2))
        }
    }

}
