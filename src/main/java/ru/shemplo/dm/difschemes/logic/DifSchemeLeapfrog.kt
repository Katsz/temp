package ru.shemplo.dm.difschemes.logic

import ru.shemplo.dm.difschemes.logic.DifferenceScheme.Scheme

@Scheme(name = "Чехарда (leapfrog)")
class DifSchemeLeapfrog(zeroLayer: DoubleArray, its: Int, u: Double, k: Double, dt: Double, dx: Double)
    : AbsDifferenceScheme(zeroLayer, its, u, k, dt, dx) {

    override fun doUnexistingStep(step: Int, profile: DoubleArray) {
        val zeroLayer = getTimeLayer(0)

        if (step == 1) {
            for (i in 1 until profile.size - 1) {
                profile[i] = (zeroLayer[i] * (1 - 2 * R)
                        + (R - S / 2) * zeroLayer[i + 1]
                        + (R + S / 2) * zeroLayer[i - 1])
            }
        } else if (step > 1) {
            val previous = arrayOf(getTimeLayer(step - 2), getTimeLayer(step - 1))

            for (i in 1 until profile.size - 1) {
                profile[i] = (previous[0][i] - previous[1][i] * 4.0 * R
                        + previous[1][i + 1] * (2 * R - S)
                        + previous[1][i - 1] * (2 * R + S))
            }
        }
    }

}
