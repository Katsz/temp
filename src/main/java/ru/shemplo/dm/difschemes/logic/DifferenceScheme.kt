package ru.shemplo.dm.difschemes.logic


interface DifferenceScheme {

    @Retention(AnnotationRetention.RUNTIME)
    @Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
    annotation class Scheme(val name: String)

    fun getTimeLayer(i: Int): DoubleArray

}
