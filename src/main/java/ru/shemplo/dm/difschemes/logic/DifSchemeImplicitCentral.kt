package ru.shemplo.dm.difschemes.logic

import ru.shemplo.dm.difschemes.logic.DifferenceScheme.Scheme

@Scheme(name = "Неявная центральная")
class DifSchemeImplicitCentral(zeroLayer: DoubleArray, its: Int, u: Double, k: Double, dt: Double, dx: Double)
    : AbsDifferenceScheme(zeroLayer, its, u, k, dt, dx) {

    override fun doUnexistingStep(step: Int, profile: DoubleArray) {
        val last = profile.size - 1
        val previous = arrayOf(getTimeLayer(step - 1))
        val equations = arrayOfNulls<DoubleArray>(last + 1)
        equations[last] = doubleArrayOf(0.0, 0.0, 1.0, previous[0][last])
        equations[0] = doubleArrayOf(0.0, 0.0, 1.0, previous[0][0])
        for (i in 1 until profile.size - 1) {
            equations[i] = doubleArrayOf(-R - S / 2, -R + S / 2, 1 + 2 * R, previous[0][i])
        }

        run3DiagonalSolver(equations, profile)
    }

}
