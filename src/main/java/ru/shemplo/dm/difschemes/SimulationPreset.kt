package ru.shemplo.dm.difschemes

import javafx.scene.control.ChoiceBox
import javafx.scene.control.TextField

import ru.shemplo.dm.difschemes.RunDifferenceSchemes.View

enum class SimulationPreset constructor(private val NAME: String, private val RENEW: () -> Unit) {

    DEFAULT("default", {
        View.U.get<TextField>().text = "-0.025"
        View.K.get<TextField>().text = "0.005"
        View.dT.get<TextField>().text = "1.0"
        View.dX.get<TextField>().text = "0.1"
        View.PROFILES.get<ChoiceBox<*>>().selectionModel
                .select(SimulationProfiles.STEP.ordinal)
    }),
    IMAGINARY_PROFILE("imaginary profile", {
        View.U.get<TextField>().text = "0.1"
        View.K.get<TextField>().text = "0.005"
        View.dT.get<TextField>().text = "1.0"
        View.dX.get<TextField>().text = "0.1"
        View.PROFILES.get<ChoiceBox<*>>().selectionModel
                .select(SimulationProfiles.HEARTBIT.ordinal)
    });

    override fun toString(): String = NAME

    fun renewGUI() = RENEW()

}
